ruby-distribution (0.8.0+dfsg-1) unstable; urgency=medium

  [ Utkarsh Gupta ]
  * Add salsa-ci.yml

  [ Debian Janitor ]
  * Use secure copyright file specification URI.
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit.
  * Update Vcs-* headers from URL redirect.
  * Use canonical URL in Vcs-Git.
  * Apply multi-arch hints.
    + ruby-distribution: Add :any qualifier for ruby dependency.
  * Use secure URI in debian/watch.
  * Bump debhelper from old 12 to 13.

  [ Cédric Boutillier ]
  * Use https:// in Vcs-* fields
  * Use new default gem2deb Rakefile to run the tests
  * Move debian/watch to gemwatch.debian.net
  * Bump Standards-Version to 4.6.2 (no changes needed)
  * New upstream version 0.8.0+dfsg
    + Remove usage of deprecated Fixnum class (ruby3.2 support)
  * Refresh packaging files
  * Remove patches

 -- Cédric Boutillier <boutil@debian.org>  Thu, 02 Nov 2023 14:21:28 +0100

ruby-distribution (0.7.3+dfsg-1) unstable; urgency=medium

  * Imported Upstream version 0.7.3+dfsg
  * Use Files-Excluded: to filter non-dfsg files
  * Update year, email, homepage in copyright file
  * Refresh packaging with dh-make-ruby -w
  * Add patches to fix or workaround issues with tests (Closes: #794165)
  * Remove coverage/ when cleaning

 -- Cédric Boutillier <boutil@debian.org>  Thu, 13 Aug 2015 10:14:29 +0200

ruby-distribution (0.7.0+dfsg-1) unstable; urgency=low

  * Initial release (Closes: #695564)
  * Repack source to remove vendor/java/commons-math-2.2.jar

 -- Cédric Boutillier <boutil@debian.org>  Tue, 11 Dec 2012 17:29:33 +0100
